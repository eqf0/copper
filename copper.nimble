# Package

version       = "0.1.0"
author        = "Edgar Quiroz"
description   = "Learning computer graphics"
license       = "MIT"
srcDir        = "src"
binDir        = "bin"
installExt    = @["nim"]
bin           = @["copper"]

# Dependencies

requires "nim >= 1.5.1"

#tasks

task cleanTests, "remove test executables":
  for testName in listFiles("tests"):
    if testName.split('.').len() == 1:
      rmFile(testName)

task tests, "run tests using testament":
  exec("testament pattern \"tests/*.nim\"")
  cleanTestsTask()

task clean, "deletes generated files":
  rmDir("testresults")
  rmDir("bin")
  rmDir("htmldocs")
  rmDir("nimcache")
  cleanTestsTask()

task docs, "generates documentation":
  exec("nim doc --project src/copper.nim")
