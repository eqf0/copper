## Toy raytracing

import space, canvas, math

func sim(u, v: Vec3): float {.inline.} =
  ## How similar the two vecrors are.
  dot(u, v)/(u.len*v.len)

func illuminate*(s: Scene, p, normal: Vec3): float =
  ## Calculares the amount of light point `p` receives on a surface with normal
  ## vector `normal`.
  result = 0
  for light in s.sources:
    result += light.intensity * (
      case light.kind:
        of directional: max(normal.sim(light.direction), 0)
        of point: max(normal.sim(light.position - p), 0)
        of ambient: 1
    )

func intersect*(s: Sphere, origin, dest: Vec3): array[2, float] =
  ## Finds solutions `t` to the intersection of sphere points
  ## `(p-s.center)^2 = s.radius^2` and the line defined by
  ## `p = origin + t*(dest-origin)`
  let
    r = s.radius # 1
    co = origin - s.center
    dir = dest - origin
    a = dot(dir, dir)
    b = 2*dot(co, dir)
    c = dot(co, co) - r*r
    d = b*b - 4*a*c

  if d < 0: return [Inf, Inf]

  [
    (-b + sqrt(d))/2*a,
    (-b - sqrt(d))/2*a
  ]

func trace*(s: Scene, origin, dest: Vec3, tMin, tMax: float): Color =
  ## Finds the color intersecting `dir` on the scene by tracing a ray of light.
  result = color(255, 255, 255)
  var closest = Inf

  # how to replace this variable?
  var closestSphere = sphere(vec3(0, 0, 0), 0, color(0, 0, 0))
  for sphere in s.objects:
    let solutions = sphere.intersect(origin, dest)
    for sol in solutions:
      if tMin <= sol and sol <= tMax and sol < closest:
        closest = sol
        closestSphere = sphere
        result = sphere.color

  if closest < Inf: # Very ugly
    let p = origin + closest*(dest - origin)
    result = s.illuminate(p, closestSphere.normal(p)) * result

proc raytrace*(c: var Canvas, s: Scene, origin = vec3(0, 0, 0)) =
  ## Finds the color of each pixel using a ray tracer and writes them to the
  ## canvas.
  for i in c.minX()..c.maxX():
    for j in c.minY()..c.maxY():
      let
        dest = s.toSceneCoords(c, pos2d(i, j))
        color = s.trace(origin, dest, 0, Inf)
      c[i, j] = color
