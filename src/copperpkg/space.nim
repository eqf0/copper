## Objects in a 3D space to raytrace/rasterize.

import canvas, math

type
  Vec3* = object
    ## Points/directions in space, elements of a vector space.
    x*, y*, z*: float

  LightKind* {.pure.} = enum
    ## Three types of light for now. Directional and ambient are simplifications
    ## to help calculations.
    point, directional, ambient

  Light* = object
    ## Light sources on the scene.
    ##
    ## Point light is like a lightbulb, expanding from a point. Technically, all
    ## light sources could be point lights.
    ##
    ## To deal with far away sources, directinal light simplifies the
    ## calculations by fixing a constant direction.
    ##
    ## Ambient light is a minumum light every point has. It is a patch not to
    ## deal with
    ## [global illumination](https://en.wikipedia.org/wiki/Global_illumination)
    case kind*: LightKind:
      of point: position*: Vec3
      of directional: direction*: Vec3
      of ambient: discard
    intensity*: float

  Sphere* = object
    ## Simplest 3D object
    center*: Vec3
    radius*: float
    color*: Color

  Scene* = ref object
    ## Region to be rendered. Also contains all objects in the space.
    objects: seq[Sphere]
    sources: seq[Light]
    width, height: int
    distance: float

func vec3*(x, y, z: float): Vec3 =
  ## Easier syntax to construct positions
  Vec3(x:x, y:y, z:z)

func `+`*(a, b: Vec3): Vec3 {.inline.} =
  ## Vector sum
  vec3(a.x+b.x, a.y+b.y, a.z+b.z)

func `*`*(k: float, p: Vec3): Vec3 {.inline.} =
  ## Product by a scalar
  vec3(k*p.x, k*p.y, k*p.z)

func `/`*(p: Vec3, k: float): Vec3 {.inline.} =
  ## Product by multiplicative inverse of an scalar
  (1/k) * p

func `-`*(a, b: Vec3): Vec3 {.inline.} =
  ## Sum of addicitve inverse
  a + (-1*b)

func dot*(a, b: Vec3): float {.inline.} =
  ## Dot product
  a.x * b.x + a.y * b.y + a.z * b.z

func len*(p: Vec3): float {.inline.} =
  ## Size of vector
  sqrt(dot(p, p))

func sphere*(center: Vec3, radius: float, color: Color): Sphere =
  ## Create sphres with easier syntax
  Sphere(center: center, radius: radius, color: color)

func normal*(s: Sphere, p: Vec3): Vec3 =
  ## Normal vector to a point on the sphere
  let dir = (p - s.center)
  dir / dir.len()

func newScene*(width, height: int, distance: float = 1): Scene {.inline.} =
  ## Crates new scene of given size and distance with no objects.
  Scene(
    objects: newSeq[Sphere](),
    width: width,
    height: height,
    distance:distance
  )

func addObj*(s: var Scene, sph: Sphere) {.inline.} =
  ## Adds sphere to the scene
  s.objects.add(sph)

func addLight*(s: var Scene, l: Light) {.inline.} =
  ## Add light source to scene
  s.sources.add(l)

iterator objects*(s: Scene): Sphere =
  ## Gets objects on scene
  for sph in s.objects: yield sph

iterator sources*(s: Scene): Light =
  ## Iterate light sources on scene
  for l in s.sources: yield l

func toSceneCoords*(s: Scene, c: Canvas, p: Pos2d): Vec3 {.inline.} =
  ## Converts canvas coordinates to the corresponding point on the scene.
  ## As the scene has a fixed distance and is parallel to the origin,
  ## this is just a scaling.
  vec3(
    p.x.float * (s.width/c.width),
    p.y.float * (s.height/c.height),
    s.distance
  )

