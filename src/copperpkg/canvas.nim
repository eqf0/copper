## Canvas with primitive access to the underling pixels.

type
  Color* = object
    # RGB colors. One byte per color.
    r*, g*, b* : byte
  Pos2d* = object
    x*, y*: int
  Canvas* = ref object
    ## Contains pixels and dimentions.
    pixels* : seq[Color]
    width, height: Natural

func color*(r, g, b: byte): Color {.inline.} =
  ## Easier color construction
  Color(r:r, g:g, b:b)

func clampByte[T](x: T): byte {.inline.} =
  ## Clamps given value to a byte range, and then performs a type conversion.
  x.clamp(0, 255).byte

func `+`*(c, d: Color): Color {.inline.} =
  # Clamped sum. Converts elements into `int`, then clamps result to `[0, 255]`
  # and converst back to `byte`.
  color(
    (c.r.int + d.r.int).clampByte,
    (c.g.int + d.g.int).clampByte,
    (c.b.int + d.b.int).clampByte
  )

func `*`*(k: float, c: Color): Color {.inline.} =
  # Clamped product. Converts fields into `float`, then clamps result to
  # `[0, 255]` and converst back to `byte`.
  color(
    (k * c.r.float).clampByte,
    (k * c.g.float).clampByte,
    (k * c.b.float).clampByte
  )

func pos2d*(x, y: int): Pos2d {.inline.} = Pos2d(x:x, y:y)

func newCanvas*(width, height: Natural): Canvas {.inline.} =
  ## New canvas. Just creates underling image.
  Canvas(
    pixels: newSeq[Color](width * height),
    width: width,
    height: height
  )

func width*(c: Canvas): int {.inline.} = c.width

func height*(c: Canvas): int {.inline.} = c.height

func minX*(c: Canvas): int {.inline.} = - (c.width div 2)

func maxX*(c: Canvas): int {.inline.} = (c.width - 1) div 2

func minY*(c: Canvas): int {.inline.} = - (c.height div 2)

func maxY*(c: Canvas): int {.inline.} = (c.height - 1) div 2

func toArrayCoords*(c: Canvas, x, y: int): Pos2d =
  ## Adjust coordinates by adding `w/2` and `h/2` respectively.
  let
    adjX = x + (c.width div 2)
    adjY = y + (c.height div 2)
  pos2d(adjX, adjY)

proc `[]`*(c: Canvas, x, y: int): Color =
  ## Returns pixel at the given position. Coordinate system with origin at the
  ## center of the canvas, unlike arrays.
  let dims = c.toArrayCoords(x, y)
  c.pixels[dims.y*c.width + dims.x]

proc `[]=`*(c: Canvas, x, y: int, color: Color) =
  ## Modifies pixel at given position. Coordinate system with origin at the
  ## center of the canvas, unlike arrays.
  let dims = c.toArrayCoords(x, y)
  c.pixels[dims.y*c.width + dims.x] = color

proc write*(c: Canvas, fileName: string) =
  ## Writes canvas to a file using a [ppm](http://netpbm.sourceforge.net/doc/ppm.html).
  ## binary format. This format is used just because it is so simple, writing
  ## binary files directly is actually simplier than using an external library.
  let file = open(fileName, fmWrite)
  defer: file.close()
  file.writeLine("P6")
  file.writeLine(c.width)
  file.writeLine(c.height)
  file.writeLine(255)
  for y in countdown(c.maxY(),c.minY()):
    for x in c.minX()..c.maxX():
      let pixel = c[x, y]
      file.write(pixel.r.chr)
      file.write(pixel.g.chr)
      file.write(pixel.b.chr)
