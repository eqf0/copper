import copperpkg/[canvas, space, raytracing]

proc addSpheres(s: var Scene) =
  s.addObj(sphere(
    center= vec3(0, -1, 3),
    radius= 1,
    color = color(255, 0, 0)
  ))
  s.addObj(sphere(
    center= vec3(2, 0, 4),
    radius= 1,
    color = color(0, 0, 255)
  ))
  s.addObj(sphere(
    center= vec3(-2, 0, 4),
    radius= 1,
    color = color(0, 255, 0)
  ))
  s.addObj(sphere(
    center = vec3(0, -5001, 0),
    color = color(255, 255, 0),
    radius = 5000
  ))

proc addLights(s: var Scene) =
  s.addLight(
    Light(kind: ambient, intensity: 0.4)
  )
  s.addLight(
    Light(
      kind: point,
      intensity: 0.3,
      position: vec3(2, 1, 0)
    )
  )
  s.addLight(
    Light(
      kind: directional,
      intensity: 0.3,
      direction: vec3(1, 4, 4)
    )
  )

when isMainModule:
  var c = newCanvas(500, 500)
  var s = newScene(1, 1)
  s.addSpheres()
  s.addLights()

  c.raytrace(s)

  c.write("imgs/test.ppm")
