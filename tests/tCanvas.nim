import copperpkg/canvas

discard """
  cmd: "nim $target --hints:on -d:testing $options $file"
"""

block oddDims:
  let c = newCanvas(23, 17)
  doAssert c.width() == 23
  doAssert c.height() == 17
  doAssert c.maxX() == 11
  doAssert c.minX() == -11
  doAssert c.maxY() == 8
  doAssert c.minY() == -8

block evenDims:
  let c = newCanvas(64, 32)
  doAssert c.maxX() == 31
  doAssert c.minX() == -32
  doAssert c.maxY() == 15
  doAssert c.minY() == -16

block arrayCoords:
  let c = newCanvas(100, 100)
  doAssert c.toArrayCoords(c.minX(), c.minY()) == pos2d(0, 0)
  doAssert (
    c.toArrayCoords(c.maxX, c.maxY) == pos2d(c.width-1, c.height-1)
  )
