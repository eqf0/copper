import copperpkg/[space, canvas]

discard """
  cmd: "nim $target --hints:on -d:testing $options $file"
"""
block scene:
  var s = newScene(10, 10)
  let sph = sphere(vec3(0, 0, 0), 1, color(0, 0, 0))
  for _ in 0..9:
    s.addObj(sph)

  for inSph in s.objects:
    doAssert inSph == sph

  let c = newCanvas(100, 100)

  doAssert (
    s.toSceneCoords(c, pos2d(c.maxX, c.maxY)) == vec3(c.maxX/10, c.maxY/10, 1)
  )
